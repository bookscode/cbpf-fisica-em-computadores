#include <iostream>

int main(int, char **)
{
    double alpha = 9.24e-3;
    double numero_moleculas = 1000;

    std::cout << "numero de moleculas: " << numero_moleculas << std::endl;

    int segundos = 75;
    for (int i = 0; i < segundos; ++i)
        numero_moleculas = numero_moleculas*(1 - alpha);

    std::cout << "numero de molulas depois de " << segundos << " segundos: " << numero_moleculas << std::endl;

    return 0;
}
