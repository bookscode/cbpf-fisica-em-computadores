#include <iostream>

void plot(int * x_array, int * y_array, std::size_t size)
{
    FILE * gnuplot = ::popen("gnuplot", "w");
    if (gnuplot == nullptr) {
        std::cerr << "Erro ao abrir o gnuplot. " << errno << std::endl;
        return;
    }

    ::fprintf(gnuplot, "plot '-' u 1:2 t 'Núcleos radioativos' w lp\n");
    for (std::size_t x = 0; x < size; ++x)
        fprintf(gnuplot,"%d %d\n", x_array[x], y_array[x]);

    ::fprintf(gnuplot, "e\n");
    ::fprintf(stdout, "Ctrl+d para sair...\n");
    ::fflush(gnuplot);
    ::getchar();

    ::pclose(gnuplot);
}

void process(int * x_array, int * y_array, std::size_t size)
{
    double alpha = 9.24e-3;
    double amostra = 1000;

    x_array[0] = 0;
    y_array[0] = amostra;

    for (std::size_t x = 1; x < size * 10; ++x) {
        amostra *= (1 - alpha);

        if (x > 1 && x % 10 == 0) {
            x_array[x / 10] = x;
            y_array[x / 10] = amostra;
        }
    }
}

int main(int, char **)
{
    constexpr std::size_t data_size = 100;

    int * x_array = new int[data_size];
    int * y_array = new int[data_size];

    process(x_array, y_array, data_size);
    plot(x_array, y_array, data_size);

    delete[] x_array;
    delete[] y_array;

    return 0;
}
